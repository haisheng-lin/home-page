import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Blog } from '@shared/models/blog';
import { Comment } from '@shared/models/comment';
import { FormControlBase } from '@shared/models/form-control-base';

import { BlogService } from '@shared/services/blog.service';
import { FormFactoryService } from '@shared/services/form-factory.service';
import { NzMessageService } from 'ng-zorro-antd';

import { Subscription } from 'rxjs';

interface FormError {
  author: string;
  content: string;
}

@Component({
  selector: 'app-blog-comment',
  templateUrl: './blog-comment.component.html',
  styleUrls: ['./blog-comment.component.less']
})
export class BlogCommentComponent implements OnInit, OnDestroy {

  @Input() blog: Blog;
  // comments: Comment[] = this.blog.comments;

  commentForm: FormGroup; // 评论表单

  formService: FormFactoryService;

  formControlBases: FormControlBase[] = [
    { key: 'author', value: '', required: true, validationMessage: { required: '请输入您的大名' } },
    { key: 'content', value: '', required: true, validationMessage: { required: '请输入评论内容' } },
  ];

  formErrors: FormError;

  // 定义 textarea 的格式
  autosize: Object = {
    minRows: 3,
    maxRows: 10
  };

  btnLoading: boolean = false; // 提交按钮是否加载中

  commentSubscription: Subscription;

  constructor (private blogService: BlogService,
    private messageService: NzMessageService) { }

  ngOnInit () {
    this.formService = new FormFactoryService();
    this.formService.initForm(this.formControlBases);
    this.commentForm = this.formService.getFormGroup();
    this.formErrors = <FormError>this.formService.getFormErrors();
  }

  ngOnDestroy () {
    if (this.commentSubscription) this.commentSubscription.unsubscribe();
  }

  /**
   * 简介：登录表单提交的处理函数
   * 
   * @return {void}
   */
  submitForm (): void {
    this.btnLoading = true;
    const comment = <Comment>this.commentForm.value;
    this.commentSubscription = this.blogService.addComment(comment, this.blog._id).subscribe(
      (blog: Blog) => {
        this.btnLoading = false;
        this.blogService.refreshEvent.emit(blog);
        this.formService.resetForm();
      }, (error: string) => {
        this.btnLoading = false;
        this.messageService.error(error);
      });
  }
}

import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { Blog } from '@shared/models/blog';
import { FormControlBase } from '@shared/models/form-control-base';

import { BlogService } from '@shared/services/blog.service';
import { FormFactoryService } from '@shared/services/form-factory.service';
import { MarkdownParserService } from '@shared/services/markdown-parser.service';
import { NzMessageService } from 'ng-zorro-antd';

import { tagOptions } from '@shared/enum';

import { Subscription } from 'rxjs';

interface FormError {
  title: string;
  tags: string;
  summary: string;
  content: string;
}

@Component({
  selector: 'app-add-blog',
  templateUrl: './add-blog.component.html',
  styleUrls: ['./add-blog.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class AddBlogComponent implements OnInit, OnDestroy {

  addForm: FormGroup;                // 添加博客表单
  tagOptions: Object[] = tagOptions; // 标签选项
  convertedText: string;             // markdown 解析的内容

  formService: FormFactoryService;

  formControlBases: FormControlBase[] = [
    { key: 'title', value: '', required: true, validationMessage: { required: '请输入博客标题' } },
    { key: 'tags', value: '', required: true, validationMessage: { required: '请选择博客标签' } },
    { key: 'summary', value: '', required: true, validationMessage: { required: '请输入博客概述' } },
    { key: 'content', value: '', required: true, validationMessage: { required: '请输入博客内容' } },
  ];

  formErrors: FormError;

  // 定义 textarea 的格式
  autosize: Object = {
    minRows: 20,
    maxRows: 30
  };

  btnLoading: boolean = false; // 提交按钮是否加载中

  addSubscription: Subscription;

  constructor (private router: Router,
    private blogService: BlogService,
    private markdownService: MarkdownParserService,
    private messageService: NzMessageService) { }

  ngOnInit () {
    this.formService = new FormFactoryService();
    this.formService.initForm(this.formControlBases);
    this.addForm = this.formService.getFormGroup();
    this.formErrors = <FormError>this.formService.getFormErrors();
  }

  ngOnDestroy () {
    if (this.addSubscription) this.addSubscription.unsubscribe();
  }

  /**
   * 简介：添加博客表单提交的处理函数
   * 
   * @return {void}
   */
  submitForm (): void {
    this.btnLoading = true;
    const data = this.parseData();
    this.addSubscription = this.blogService.addBlog(data)
      .subscribe(() => {
        this.btnLoading = false;
        this.formService.resetForm();
        this.router.navigate(['/']);
      }, (error: string) => {
        this.btnLoading = false;
        this.messageService.error(error);
      });
  }

  /**
   * 简介：主要把 location 数组转化成省份、城市两个字段，如果遇到直辖市或特别行政区，则省份与城市一致
   * 
   * @return {Blog}
   */
  parseData (): Blog {
    const data = new Blog();
    data.title = this.addForm.value['title'];
    data.tags = this.addForm.value['tags'];
    data.summary = this.addForm.value['summary'];
    data.content = this.addForm.value['content'];
    return data;
  }

  /**
   * 简介：根据输入的 markdown 解析内容，更新预览
   * 
   * @param  {string} mdText markdown 内容
   * @return {void}
   */
  updatePreview (mdText: string): void {
    this.convertedText = this.markdownService.convert(mdText);
  }
}

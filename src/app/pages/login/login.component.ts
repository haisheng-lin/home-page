import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { FormControlBase } from '@shared/models/form-control-base';

import { AdminAuthService } from '@shared/services/admin-auth.service';
import { FormFactoryService } from '@shared/services/form-factory.service';
import { NzMessageService } from 'ng-zorro-antd';

import { Subscription } from 'rxjs';

import * as CryptoJS from 'crypto-js';

interface FormError {
  adminName: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit, OnDestroy {

  loginForm: FormGroup;       // 登录表单
  showModal: boolean = false; // 弹窗是否显示

  formService: FormFactoryService;

  formControlBases: FormControlBase[] = [
    { key: 'adminName', value: '', required: true, validationMessage: { required: '请输入名称' } },
    { key: 'password', value: '', required: true, validationMessage: { required: '请输入密码' } },
  ];

  formErrors: FormError;

  btnLoading: boolean = false; // 提交按钮是否加载中

  modalSubscription: Subscription;
  loginSubscription: Subscription;

  constructor (private adminAuthService: AdminAuthService,
    private messageService: NzMessageService) { }

  ngOnInit () {
    this.formService = new FormFactoryService();
    this.formService.initForm(this.formControlBases);
    this.loginForm = this.formService.getFormGroup();
    this.formErrors = <FormError>this.formService.getFormErrors();
    this.modalSubscription = this.adminAuthService.loginEvent.subscribe(
      () => { this.showModal = true; }
    );
  }

  ngOnDestroy () {
    if (this.modalSubscription) this.modalSubscription.unsubscribe();
    if (this.loginSubscription) this.loginSubscription.unsubscribe();
  }

  /**
   * 简介：登录表单提交的处理函数
   * 
   * @return {void}
   */
  submitForm (): void {
    this.btnLoading = true;
    const adminName = this.loginForm.value['adminName'];
    const password = this.loginForm.value['password'];
    const hashPwd = CryptoJS.SHA256(password).toString(); // 密码需要经过 SHA256 加密
    this.loginSubscription = this.adminAuthService.login(adminName, hashPwd).subscribe(
      (adminName: string) => {
        this.btnLoading = false;
        this.closeModal();
        this.messageService.success(`Welcome back, ${adminName}`);
      }, (error: string) => {
        this.btnLoading = false;
        this.messageService.error(error);
      });
  }

  /**
   * 简介：点击关闭弹窗的处理函数
   * 
   * @return {void}
   */
  closeModal (): void {
    this.showModal = false;
  }
}

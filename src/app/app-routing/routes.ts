import { Routes } from '@angular/router';

import { HomeComponent } from '@pages/home/home.component';
import { BlogListComponent } from '@pages/blog-list/blog-list.component';
import { BlogDetailComponent } from '@pages/blog-detail/blog-detail.component';
import { PageNotFoundComponent } from '@pages/page-not-found/page-not-found.component';

import { AdminComponent } from '@pages/admin/admin.component';
import { BlogManagementComponent } from '@pages/admin/blog-management/blog-management.component';
import { AddBlogComponent } from '@pages/admin/blog-management/add-blog/add-blog.component';

import { AdminAuthService } from '@shared/services/admin-auth.service';

export const clientRoutes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' }, // 根路径默认跳转至 home

  { path: 'home', component: HomeComponent },               // 首页
  { path: 'blog', component: BlogListComponent },           // 博客列表
  { path: 'blog/:blogId', component: BlogDetailComponent }, // 博客详情

  { path: '404', component: PageNotFoundComponent },        // 404 页面
  { path: '**', redirectTo: '/404' },                       // 路由不匹配重定向到 404
];

export const adminRoutes: Routes = [
  { // 管理员管理路由
    path: 'admin', component: AdminComponent, canActivate: [AdminAuthService], children: [
      { path: '', redirectTo: 'blog', pathMatch: 'full' }, // 默认重定向到博客管理
      // 博客管理
      { path: 'blog', component: BlogManagementComponent,
        children: [
          { path: '', redirectTo: 'create', pathMatch: 'full' }, // 默认重定向到添加博客
          { path: 'create', component: AddBlogComponent },
        ]
      }
    ]
  }
];
import { ValidatorFn } from "@angular/forms";

// 参考文档：https://angular.cn/guide/dynamic-form
export class FormControlBase {

  key: string;                      // 字段名，必填且必须唯一
  label?: string;                   // 描述说明
  value?: any;                      // 初始值
  required?: boolean;               // 是否必填
  customValidators?: ValidatorFn[]; // 自定义校验函数
  validationMessage?: Object;       // 发生错误时的提示信息, e.g. { required: '请输入信息', reg: '格式不合法' }

  constructor (options: {
    value?: any,
    key: string,
    label?: string,
    required?: boolean,
    validationMessage?: Object,
    customValidators?: ValidatorFn[]
  }) {
    this.key = options.key;
    this.label = options.label;
    this.value = options.value;
    this.required = options.required;
    this.validationMessage = options.validationMessage;
    this.customValidators = options.customValidators;
  }
}
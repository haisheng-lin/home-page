import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Response } from '@shared/models/response';

import { ProcessHttpMsgService } from '@shared/services/process-http-msg.service';

import { environment } from '@environments/environment';

import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DaoService {

  rootUrl = environment.apiUrl; // 后端接口根地址

  constructor (private http: HttpClient,
    private processHttpMsgService: ProcessHttpMsgService) { }

  /**
   * 获取数据
   * 
   * @param  {string} url 接口相对地址，必须以 '/' 开头
   * @return {Observable<Object>}
   */
  get (url: string): Observable<Object> {
    return this.http.get<Response>(`${this.rootUrl}${url}`)
      .pipe(
        map(this.processHttpMsgService.handleResponse),
        catchError(this.processHttpMsgService.handleError)
      );
  }

  /**
   * 添加数据
   * 
   * @param  {string} url 接口相对地址，必须以 '/' 开头
   * @param  {Object} data 新增的数据
   * @return {Observable<any>}
   */
  post (url: string, data: Object): Observable<any> {
    return this.http.post<Response>(`${this.rootUrl}${url}`, data)
    .pipe(
      map(this.processHttpMsgService.handleResponse),
      catchError(this.processHttpMsgService.handleError)
    );
  }

  /**
   * 修改数据
   * 
   * @param  {string} url 接口相对地址，必须以 '/' 开头
   * @param  {Object} data 新增的数据
   * @return {Observable<any>}
   */
  put (url: string, data: Object): Observable<any> {
    return this.http.put<Response>(`${this.rootUrl}${url}`, data)
    .pipe(
      map(this.processHttpMsgService.handleResponse),
      catchError(this.processHttpMsgService.handleError)
    );
  }

  /**
   * 删除数据
   * 
   * @param  {string} url 接口相对地址，必须以 '/' 开头
   * @return {Observable<any>}
   */
  delete (url: string): Observable<any> {
    return this.http.delete<Response>(`${this.rootUrl}${url}`)
    .pipe(
      map(this.processHttpMsgService.handleResponse),
      catchError(this.processHttpMsgService.handleError)
    );
  }
}

import { TestBed, inject } from '@angular/core/testing';

import { FormFactoryService } from './form-factory.service';

describe('FormFactoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FormFactoryService]
    });
  });

  it('should be created', inject([FormFactoryService], (service: FormFactoryService) => {
    expect(service).toBeTruthy();
  }));
});

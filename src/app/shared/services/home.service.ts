import { Injectable, EventEmitter } from '@angular/core';

import { HomeData } from '@shared/models/home-data';
import { Experience } from '@shared/models/experience';
import { DaoService } from '@shared/services/dao.service';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  url: string = '/home'; // 后端接口地址

  showExpDetailEvent: EventEmitter<Experience>; // 打开工作经历详情弹窗的事件

  constructor (private daoService: DaoService) {
    this.showExpDetailEvent = new EventEmitter();
  }

  /**
   * 简介：获取首页的数据，如：工作经历、教育背景等
   * 
   * @return {Observable<HomeData>}
   */
  getHomeData (): Observable<HomeData> {
    return this.daoService.get(this.url)
      .pipe(
        map((data: Object) => { return <HomeData>data }),
      );
  }
}

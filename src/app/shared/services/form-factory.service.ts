import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

import { FormControlBase } from '@shared/models/form-control-base';

@Injectable({
  providedIn: 'root'
})
export class FormFactoryService {

  // 注意: 这个不能作为依赖注入，否则所有组件都会共享，造成数据错误

  private form: FormGroup;                 // 表单对象
  private formErrors: Object = {};         // 记录表单输入错误的对象
  private validationMessages: Object = {}; // 表格输入的相关错误提示信息

  private formControlBases: FormControlBase[];

  constructor () { }

  /**
   * 初始化表单, 错误对象, 错误提示信息
   * 
   * @param  {FormControlBase[]} formControlBases 
   * @return {void}
   */
  initForm (formControlBases: FormControlBase[]): void {

    this.formControlBases = formControlBases;

    let group: any = {};

    formControlBases.forEach((formControlBase: FormControlBase) => {

      // 1. 处理 FormControl
      group[formControlBase.key] = this.parseFormControl(formControlBase);

      // 2. 构建表单错误对象与信息
      this.formErrors[formControlBase.key] = '';
      this.validationMessages[formControlBase.key] = formControlBase.validationMessage;
    });

    this.form = new FormGroup(group);
    this.form.valueChanges.subscribe(data => this.onFormChanged(data));
  }

  /**
   * 重置表单
   * 
   * @return {void}
   */
  resetForm (): void {
    const resetObject = {};
    this.formControlBases.forEach((formControlBase: FormControlBase) => {
      resetObject[formControlBase.key] = formControlBase.value;
    });
    this.form.reset(resetObject);
  }

  /**
   * 设置表单
   * 
   * @return {void}
   */
  setForm (resetObject: Object): void {
    this.form.reset(resetObject);
  }

  /**
   * 当表单输入改变时触发的处理函数
   * 
   * @param  {any} data 表单数据
   * @return {void}
   */
  onFormChanged (data?: any): void {

    if (!this.form) return;

    for (const field in this.formErrors) { // 检查所有 field
      this.formErrors[field] = ''; // 如果之前有错误信息则清空
      const control = this.form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          if (this.formErrors[field].length > 0) {
            this.formErrors[field] += '，';
          }
          this.formErrors[field] += messages[key];
        }
      }
    } 
  }

  /**
   * 动态添加 FormControl
   * 
   * @param  {FormControlBase} formControlBase
   * @return {void}
   */
  addControl (formControlBase: FormControlBase): void {

    // 1. 增加 FormControl
    const control = this.parseFormControl(formControlBase);
    this.form.addControl(formControlBase.key, control);

    // 2. 增加表单错误对象与信息
    this.formErrors[formControlBase.key] = '';
    this.validationMessages[formControlBase.key] = formControlBase.validationMessage;
  }

  /**
   * 动态删除 FormControl
   * 
   * @param  {FormControlBase} formControlBase
   * @return {void}
   */
  removeControl (formControlBase: FormControlBase): void {

    // 1. 删除 FormControl
    this.form.removeControl(formControlBase.key);

    // 2. 删除表单错误对象与信息
    delete this.formErrors[formControlBase.key];
    delete this.validationMessages[formControlBase.key];
  }

  /**
   * 获取表单对象
   * 
   * @return {FormGroup}
   */
  getFormGroup (): FormGroup {
    return this.form;
  }

  /**
   * 获取表单错误对象
   * 
   * @return {Object}
   */
  getFormErrors (): Object {
    return this.formErrors;
  }

  /**
   * 把 FormControlBase 转化为 AbstractControl
   * 
   * @param  {FormControlBase} formControlBase
   * @return {AbstractControl}
   */
  private parseFormControl (formControlBase: FormControlBase): AbstractControl {

    const validators = formControlBase.customValidators || [];
    if (formControlBase.required) {
      validators.push(Validators.required);
    }

    return new FormControl(formControlBase.value, validators);
  }
}

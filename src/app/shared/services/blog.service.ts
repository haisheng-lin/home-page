import { Injectable, EventEmitter } from '@angular/core';

import { Blog } from '@shared/models/blog';
import { Comment } from '@shared/models/comment';
import { DaoService } from '@shared/services/dao.service';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  url: string = '/blogs'; // 后端接口地址

  refreshEvent: EventEmitter<Blog>; // 刷新博客事件

  constructor (private daoService: DaoService) {
    this.refreshEvent = new EventEmitter();
  }

  /**
   * 简介：获取博客列表信息
   * 
   * @return {Observable<Blog[]>}
   */
  getBlogs (): Observable<Blog[]> {
    return this.daoService.get(this.url)
      .pipe(
        map((data: Object) => { return <Blog[]>data['blogs'] }),
      );
  }

  /**
   * 简介：根据博客 ID 获取博客详情
   * 
   * @param  {string} blogId 博客 ID
   * @return {Observable<Blog>}
   */
  getBlogById (blogId: string): Observable<Blog> {
    return this.daoService.get(`${this.url}/${blogId}`)
      .pipe(
        map((data: Object) => { return <Blog>data['blog'] }),
      );
  }

  /**
   * 简介：添加博客
   * 
   * @param  {Blog} blog 博客数据
   * @return {Observable<any>}
   */
  addBlog (blog: Blog): Observable<any> {
    return this.daoService.post(this.url, blog);
  }

  /**
   * 简介：给博客添加评论
   * 
   * @param  {Comment} comment 评论数据
   * @param  {string} blogId 博客 ID
   * @return {Observable<any>}
   */
  addComment (comment: Comment, blogId: string): Observable<Blog> {
    return this.daoService.post(`${this.url}/${blogId}/comments`, comment)
      .pipe(
        map((data: Object) => { return <Blog>data['blog']; }),
      );
  }
}
